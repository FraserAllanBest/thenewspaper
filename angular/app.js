var nponline = false;
var linkster = '';
if (!nponline) {
	linkster = 'newspaper/';
}

//////////* Dependencies *//////////

//// Plugins ////
// @koala-prepend "plugins/jQuery.js"
// @koala-prepend "plugins/jQueryPlugins.js"
// @koala-prepend "plugins/editable.min.js"
// @koala-prepend "plugins/jquery-ui.js"


//// Modules ////
// @koala-prepend "modules/angular-route.js"
// @koala-prepend "modules/ng-file-upload-all.js"

//// Controllers ////
// @koala-append "controllers/MainController.js"
// @koala-append "controllers/FrontController.js"
// @koala-append "controllers/ProfileController.js"
// @koala-append "controllers/ArticleController.js"
// @koala-append "controllers/LoginController.js"
// @koala-append "controllers/SignupController.js"

//// Services ////
// @koala-append "services/userService.js"
// @koala-append "services/sessionService.js"
// @koala-append "services/articleService.js"
// @koala-append "services/imageService.js"
// @koala-append "services/searchService.js"

//// Directives ////
// @koala-append "directives/articleDirective.js";
// @koala-append "directives/headerDirective.js";
// @koala-append "directives/searchDirective.js";
// @koala-append "directives/catalogueDirective.js";
// @koala-append "directives/pannelDirective.js";
// @koala-append "directives/verticalDirective.js";


var newspaper = angular.module("newspaper",['ngRoute','ngFileUpload'])
.config(['$routeProvider','$locationProvider', function($routeProvider,$locationProvider) { 
	$routeProvider.when('/',{
		templateUrl:'../' + linkster + 'angular/views/main.html',
		controller:'FrontController'
	})
	.when('/profile/:name',{
		templateUrl:'../' + linkster + 'angular/views/profile.html',
		controller:'ProfileController'
	})
	.when('/article/:title',{
		templateUrl:'../' + linkster + 'angular/views/article1.html',
		controller:'ArticleController'
	})
	.when('/login',{
		templateUrl:'../' + linkster + 'angular/views/login.html',
		controller:'LoginController'
	})
	.when('/signup',{
		templateUrl:'../' + linkster + 'angular/views/signup.html',
		controller:'SignupController'
	})
	.otherwise({ redirectTo: '/' });
	/*
	if(window.history && window.history.pushState){
    	$locationProvider.html5Mode(true);
  	}
  	*/
}])
.run(['$rootScope',function($rootScope) {
    $rootScope.main = {title:'',loaded:false,loggedin:false, articleRepo : [], hash:''};
    $rootScope.$on("$routeChangeStart",function(){

    	if ($rootScope.main.online) {
    		$rootScope.main.news = ''
    	}

		$rootScope.main.loaded = false;
		$rootScope.main.loggedin = false;
	});
	/*
	if(window.history && window.history.pushState){
		$rootScope.main.hash = '#/';
	}
	*/
}]);