newspaper.controller("ArticleController", ['$scope','$routeParams','articleService','imageService', function($scope,$routeParams,articleService,imageService){
	// Article Info //
	$scope.displayArticle = {id:0, title:'',author:'', description: '', content:'', nodes:[] ,date:'', state:'', link:'',images:'',section:'',featured:false};	
	$scope.format = {newImg : ''};
	// Title Of 
	$scope.main.title = 'The Newspaper' + $scope.displayArticle.title;
	$scope.newImage = {title: '' , info:'Upload Article!'};

	// Editor Pannel //
	$scope.editorPanel = {state:1};
	
	// Article Onload //
	$scope.$on("$routeChangeSuccess",function(){
		articleService.get($routeParams.title,$scope);
		$scope.searchy.value = 'other';
	});
	// Save Article //
	$scope.save = function(){
		articleService.save($scope);
	};
	$scope.insertImg = function(img) {
		$scope.format.newImg = img;
	};
	// toggle featured
	$scope.toggleFeatured = function(){
		$scope.displayArticle.featured = !$scope.displayArticle.featured;
		console.log($scope.displayArticle.featured);
	};
	//Upload Images
	$scope.uploadImage = function (file) {
		if (file != null && !file.$error) {
			imageService.set(file,$scope);
		}
	};
}]);