newspaper.controller("FrontController", ['$scope','searchService',function($scope,searchService){
	$scope.main.title = 'The Newspaper';
	$scope.$on('$routeChangeSuccess',function(){
		searchService.generate($scope);
	});
}]);