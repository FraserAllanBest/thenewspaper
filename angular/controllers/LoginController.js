newspaper.controller("LoginController", ['$scope','userService', function($scope, userService){
	$scope.main.title = 'The Newspaper : Login';
	$scope.main.loaded = true;
	$scope.oldUser = {email:'', password:''};
	$scope.msgtxt = '';
	$scope.login = function() {
		userService.login($scope);
	}
	$scope.$on('$routeChangeSuccess',function(){
		$scope.searchy.value = 'other';
	});
}]);