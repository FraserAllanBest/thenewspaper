newspaper.controller("MainController",['$scope','searchService',function($scope,searchService){
	$scope.theme = {};
	$scope.searchy = {value:'main',nodes:[]};
	$scope.header = {sections:['News','Opinion','Column','Inside','Video','Another']};
	$scope.page = {rows:5,space:15,stage:0,stages:0,content:[], main:[], recent:[], featured:[]};

	$scope.setSection = function(sect) {
		$scope.searchy.value = sect;
		$('.search-wrapper').find('.search').html(sect);
	}
}]);