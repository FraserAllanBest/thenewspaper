newspaper.controller("ProfileController", ['$scope','$routeParams','userService','articleService',function($scope,$routeParams,userService,articleService){
	$scope.main.title = "The Newspaper : Profile";
	
	$scope.profileUser = { first: '', last: '', title: '', articles: []};
	$scope.articleInfo = {title:'', description:''};
	
	$scope.$on('$routeChangeSuccess',function(){
		userService.get($routeParams.name , $scope);
		$scope.searchy.value = 'other';
	});
	
	$scope.newArticle = function() {
		articleService.set($scope);
	};
}]);