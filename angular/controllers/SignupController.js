newspaper.controller("SignupController", ['$scope','userService',function($scope,userService){
	$scope.main.title = 'The Newspaper : Sign Up';
	$scope.main.loaded = true;
	$scope.newUser = {first:'', last:'', title:'', email:'', password1:'', password2:''};
	$scope.msgtxt = '';
	$scope.signup = function () {
		userService.set($scope);
	}
}]);