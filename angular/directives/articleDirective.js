newspaper.directive('npArticle', ['$sce','$parse' ,'imageService','$rootScope', function($sce,$parse,imageService,$rootScope) {
    return {
        restrict: 'AE',
        //require: 'ngModel',
        //scope: true;
        templateUrl: linkster + 'angular/directives/Templates/articleTemplate.html',
        replace:true,
        link: function(scope, element, attrs) {
	        var editor = $(element).find('#editor')[0];
	        
	        var medium = new Medium({
			    element: editor,
			    mode: Medium.richMode,
                autofocus: true,
                autoHR: false,
                maxLength: -1,
                modifiers: {
                    'b': 'bold',
                    'i': 'italicize',
                    'u': 'underline',
                    'v': 'paste'
                },
                pasteAsText: false,
                beforeInvokeElement: function () {
					//this = Medium.Element
					console.log(this);
				},
				beforeInsertHtml: function () {
					
					//console.log(this.html.nodeType);
					
					if (this.html.nodeType != 1) {
						console.log(this);
						var html = $.parseHTML(this.html);
						
						
						if (html[0].nodeType == 3) {
							var newHtml = ''
							
							for (var i = 0; i < html.length; i++){
								//console.log(html[i]);
								if ($(html[i]).prop('outerHTML') != undefined) {
									if ($(html[i]).prop('outerHTML') != '<br>'){
										newHtml += $(html[i]).prop('outerHTML');
									}
								}
								else if (html[i].nodeValue != null){
		        					newHtml += ('<p>' + html[i].nodeValue + '</p>');
		        				}
		        			}
		        			this.html = newHtml;
		        		}
	        		}
	        		


	       	 
					//console.log(html);
				},
				beforeAddTag: function (tag, shouldFocus, isEditable, afterElement) {
					console.log('hey');
				}
			});

	        /*
			$('#editor').sortable({
				
				axis:"y",
				containment: "parent",
				items: "img"
			});*/
			/*
			scope.uploadImage = function (file) {
				if (file != null && !file.$error) {
					imageService.set(file,$scope);
				}
			}*/
			
			scope.$watch('displayArticle.images',function(newValue,oldValue){
				if ((newValue != oldValue)){
					console.log('New image added');
					var img = document.createElement('img');
					img.setAttribute('src', newValue[newValue.length-1]);

					medium.focus();
					$('#editor').focus();
					medium.insertHtml(img);
					// compile after
					scope.save();
					//img.setAttribute('ng-src', newValue);
					return false;
					
				}
				/*
	        	if (newValue != oldValue && newValue!= '') {
			        var img = document.createElement('img');
			        img.setAttribute("_moz_resizing", "false")
			        img.className = 'img-responsive';
			        img.setAttribute('ng-src', newValue);
			        $compile(img)(scope)
			        medium.focus();
			        medium.insertHtml(img);
			        scope.format.newImg = '';
			        scope.save();
				}*/
				
			},true);
			

	    
	    }
    }
}]);