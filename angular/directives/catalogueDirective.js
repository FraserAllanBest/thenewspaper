newspaper.directive('npCatalogue',['$interval',function($interval){
	return {
		restrict: 'EA',
		templateUrl: linkster + 'angular/directives/Templates/catalogueTemplate.html',
		replace: true,
		scope:true,
		link: function(scope,element,attrs){
			$interval(
				function() {
					$('.recent-row').height($('.main-row').height());
				},100,-1
			);
		}
	}
}]);