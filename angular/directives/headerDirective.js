newspaper.directive('npHeader',['$interval', function($interval){
	return {
		restrict: 'EA',
		templateUrl: linkster + 'angular/directives/Templates/headerTemplate.html',
		replace: true,
		scope: true,
		link: function(scope,element,attrs){
			

			$interval(
				function(){	
					for(var i = 0; i < $('.link-text').length; i++){
						$($('.link-text')[i]).verticalAlign();
					}
					$('#logo1').verticalAlign();
					$('#sandwich').verticalAlign();
				},
				25,100
			);
			

		    $(window).resize(function(){
		    	for(var i = 0; i < $('.link-text').length; i++){
					$($('.link-text')[i]).verticalAlign();
				}
				$('#logo1').verticalAlign();
				$('#sandwich').verticalAlign();
		    });
		}
	}
}]);