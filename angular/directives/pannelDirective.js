newspaper.directive('npPannel',[function(){
	return {
		restrict:'EA',
		templateUrl: linkster + 'angular/directives/Templates/pannelTemplate.html',
		replace:true,
		scope:{
			num:'=num',
			content:'=content'
		},
		link: function(scope,element,attrs){
			var classes = ['one','two','three','four'];
			element.addClass(classes[scope.num-1]);
			if (typeof scope.content.images[0] == 'undefined') {
				scope.content.images.push(linkster+'bootstrap/images/r7Z09Ht.gif');
			}
		}
	}
}])