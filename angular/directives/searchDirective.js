newspaper.directive('npSearch', ['searchService','$route','$location', function(searchService,$route,$location) {
    return {
        restrict: 'AE',
        templateUrl: linkster + 'angular/directives/Templates/searchTemplate.html',
        replace: true,
        require: 'ngModel',

        link: function(scope, element, attrs, ctrl) {

        	var searchElement = $(element).find('.search');

	        var searchMedium = new Medium({
			    element: searchElement[0],
			    mode: Medium.inlineMode,
			    maxLength: 100
			});
			
			function isHTML(str) {
    			var a = document.createElement('div');
    			a.innerHTML = str;
    			for (var c = a.childNodes, i = c.length; i--; ) {
        			if (c[i].nodeType == 1) return true; 
    			}
    			return false;
			}
			
			scope.$watch('searchy.value',function(newVal,oldVal){
				// if space or enter or ten seconds have past
				scope.searchy.nodes = newVal.split(' ');
				if ((newVal != oldVal) && (newVal != 'other') ){
					// front => refresh route
					if($location.path() == '/'){
						$route.reload();
					// !front => redirect to front
					} else {
						$location.path('/');
					}
				}
			});

		    element.bind('keyup', function() {
		        scope.$apply(function() {
		        	var values = searchElement.prop('innerHTML').replace(/&nbsp;/g,'')
		        								   .replace('<br>','')
		        								   .replace('NaN','')
		        								   .split(/\b\s+(?!$)/);
		        	var nodeList = [];
		        	for (var i = 0; i < values.length; i++) {
		        		if (isHTML(values[i])){
		        			nodeList.push(values[i]);
		        		}
		        	}
		        	var newInput = values.join(' ');
		        	scope.searchy.value = newInput;
		    	});
		    });
        }
    }
}]);