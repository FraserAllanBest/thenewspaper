newspaper.factory('articleService', ['$http','$location','$sce','sessionService', function($http, $location, $sce, sessionService) {
	return {
		set: function (scope) {
			var packet = {id: sessionService.get('id'), contents: scope.articleInfo};
			var $promise = $http.post(linkster + 'index.php/article/set',packet);
			$promise.then(function(response){
				if (response.data) {
					console.log(response.data);
					if (!response.data.success) {
						//console.log('server error in set article');
						//$location.path('/profile');
					} else {
						if(response.data.package.logged == true) {
							scope.main.loggedin = true;
						}
						$location.path('/article/'+response.data.package.link);
					}
				} else {
					console.log('server error in set');
					//$location.path('/profile');
				}
			});
		},

		get: function(url,scope) {
			// Called in Editor Controller
			var packet = {id: sessionService.get('id'), contents: url };
			$promise = $http.post(linkster + 'index.php/article/get',packet);
			$promise.then(function(response){
				if(response.data) {
					if (!response.data.success) {
						console.log('server error in get article');
						$location.path('/profile');
					} else {
						if(response.data.package.logged == true) {
							scope.main.loggedin = true;
						}
						console.log(response.data.package.article.content);
						scope.displayArticle.id = response.data.package.article.id;
						scope.displayArticle.title = response.data.package.article.title;
						scope.displayArticle.description = response.data.package.article.description;
						scope.displayArticle.content = $sce.trustAsHtml(response.data.package.article.content);
						scope.displayArticle.author = response.data.package.article.author;
						scope.displayArticle.link = response.data.package.article.link;
						scope.displayArticle.state = response.data.package.article.state;
						scope.displayArticle.date = response.data.package.article.date;
						scope.displayArticle.root = response.data.package.article.root;
						scope.displayArticle.images = response.data.package.article.images;
						scope.displayArticle.section = response.data.package.article.section;
						scope.displayArticle.featured = response.data.package.article.featured;
						
						
					}
					scope.main.loaded = true;
				} else {
					console.log('server error in get');
					//$location.path('/profile');
				}
			});
		},
		
		save: function(scope){
			scope.displayArticle.content = $('#editor').prop('innerHTML');
			console.log(scope.displayArticle.content);
			//console.log(scope.displayArticle.content);
			var packet = {id: sessionService.get('id'), contents: scope.displayArticle};
			$promise = $http.post(linkster + 'index.php/article/save',packet);
			$promise.then(function(response){
				if(response.data) {
					console.log(response.data);
					if (!response.data.success) {
						console.log('saved!');
					} else {
						
					}
				} else {
					console.log('server error');
				}
			
			});
		},
		
		/*
		newsave: function(scope, oldVal, newVal) {
			var newHTML = $(newVal) , oldHTML = $(oldVal);
			var contrast = this.changes(newHTML,oldHTML);
			var packet = {id: sessionService.get('id'), contents: {change : contrast, link: scope.displayArticle.link} };
			$promise = $http.post('index.php/article/save',packet);
			$promise.then(function(response){
				if(response.data) {
					if (!response.data.success) {
						
					} else {
						
					}
				} else {
					
				}
			});
		},
		*/
		
		updateContent: function(scope){
			scope.displayArticle.content = $sce.trustAsHtml($('#content').prop('innerHTML'));
			console.log('content change');
			
		},
		makeNodes: function(scope) {
			var nodes = $(scope.displayArticle.content);
			/*
			//if (!(nodes[0] instanceof HTMLElement)) {
				var convNodes = [];
				for (var i = 0; i < nodes.length; i++){
					console.log(nodes[i] instanceof HTMLElement);
					convNodes.push($(nodes[i]).prop('outerHTML'));
				}
				scope.displayArticle.nodes = convNodes;
			//}
			*/
		},
		
		changes: function(newNode, oldNode) {
			if ((newNode[0] instanceof HTMLElement) && (oldNode[0] instanceof HTMLElement)) {
				var i, nodeList = [], fLen = newNode.length, sLen = oldNode.length, len;
			    if (fLen > sLen) {
			        len = sLen;
			    } else if (fLen < sLen) {
			        len = fLen;
			    } else {
			        len = sLen;
			    }
			    for ( i = 0; i < len; i++) {
			    	if ((newNode[i] instanceof HTMLElement) && (oldNode[i] instanceof HTMLElement)) {
				        if (!newNode[i].isEqualNode(oldNode[i])) {
				            nodeList.push({idx: i, elem1: $(newNode[i]).prop('outerHTML'), elem2: $(oldNode[i]).prop('outerHTML')});  //idx: array index
				        }
			    	}
			    }
			    if (fLen > sLen) {  // first > second
			        for (i = sLen; i < fLen; i++) {
			            nodeList.push({idx: i, elem1: $(newNode[i]).prop('outerHTML'), elem2: 'undefined'});
			        }
			    } else if (fLen < sLen) {
			        for (i = fLen; i < sLen; i++) {
			            nodeList.push({idx: i, elem1: 'undefined', elem2: $(oldNode[i]).prop('outerHTML')});
			        }
			    }    
			    return nodeList;
			}
		}
	}
}]);