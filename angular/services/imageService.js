newspaper.factory('imageService', ['$http','$location','$sce','sessionService','articleService','Upload','$timeout','$compile', function($http, $location, $sce, sessionService,articleService, Upload, $timeout,$compile) {
	return {
		
		set: function(file,scope){
	        Upload.upload({
	            url: linkster + 'index.php/image/imageUpload',
	            fields: {'id' : sessionService.get('id'), 'title' : scope.newImage.title , 'link': scope.displayArticle.link},
	            file: file,
	            method: 'POST'
	        }).progress(function (evt) {
	            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	            scope.newImage.info = 'progress: ' + progressPercentage + '% ' + evt.config.file.name ;
	        }).success(function (data, status, headers, config) {
		        if (data){
			        scope.newImage.info = "Recieving Response...";
			        if(data.success == true) {
				        scope.newImage.info = "Image is Uploaded!";
				        scope.displayArticle.images.push(data.package.imglink);
						scope.save();
				        $timeout(function(){scope.newImage.info = "Upload another image by clicking choose file again"},8000);
				        return data.package.imglink;
					} else {
						scope.newImage.info = $(data.error).text();
						$timeout(function(){scope.newImage.info = "Upload another image by clicking choose file again"},8000);
					}
				} else {
					scope.newImage.info = "Server Error, Try Again!";
					$timeout(function(){scope.newImage.info = "Upload another image by clicking choose file again"},8000);
				}
	        }).error(function (data, status, headers, config) {
	            console.log('error status: ' + status);
	            scope.newImage.info = "Server Error, Try Again!";
	            $timeout(function(){scope.newImage.info = "Upload another image by clicking choose file again"},8000);
	        });
		},
		
		insert: function(scope,link) {
			// create image,  compile image

			// append to edit area
		},
		
		/*
		imageRoll: function(scope) {
			//append  image to avalible images
		},
		*/
		compile: function(scope) {
			
		}
	}
}]);