newspaper.factory('searchService', ['$filter','sessionService','$http',function($filter,sessionService,$http) {
	return{
		generate: function(scope) {
			// /console.log(scope.page.stages);
			// Called in Editor Controller
			var thisy = this;
			var packet = {id: sessionService.get('id'), contents: {terms: scope.searchy.nodes, stages: scope.page.stages, space: scope.page.space}};
			$promise = $http.post(linkster + 'index.php/search/get',packet);
			$promise.then(function(response){
				if(response.data) {
					if (!response.data.success) {

						//console.log('server error in get article');
						//$location.path('/profile');
					} else {
						scope.page.stage = 0;
						scope.page.content = response.data.package.main;
						scope.page.recent = response.data.package.recent;
						scope.page.stages = Math.ceil(response.data.package.main.length/scope.page.rows);
						console.log(scope.page.recent);
						console.log(scope.page.content);
						thisy.render(scope);
						//if(response.data.package.logged == true) {
						//	scope.main.loggedin = true;
						//}
						
					}
					//scope.main.loaded = true;
				} else {
					//console.log('server error in get');
				}
				
			});
		},

		addition: function(scope) {
			scope.page.content.push(
				{id:0, title:'Lorem',author:'chris b', description: '', content:'hello', nodes:[] ,date:'', state:'pub', link:'',images:[]},
				{id:1, title:'ipsum',author:'chris b', description: '', content:'hello', nodes:[] ,date:'', state:'pub', link:'',images:[]},
				{id:2, title:'dollar',author:'chris b', description: '', content:'hello', nodes:[] ,date:'', state:'pub', link:'',images:[]},
				{id:3, title:'duck',author:'chris b', description: '', content:'hello', nodes:[] ,date:'', state:'pub', link:'',images:[]},
				{id:4, title:'goose',author:'chris b', description: '', content:'hello', nodes:[] ,date:'', state:'pub', link:'',images:[]},
				{id:5, title:'flipper',author:'chris b', description: '', content:'hello', nodes:[] ,date:'', state:'pub', link:'',images:[]}
			);
			// the section your in, other metadata
			// get stage in pagination
			// get articles, add to pagination stage num of
			// requested articles
		},
		render:function(scope) {
			scope.page.main = scope.page.content.slice(scope.page.stage*scope.page.rows,scope.page.stage*scope.page.rows+scope.page.rows+1);
			scope.main.loaded = true;
		},
		store: function(scope) {

		}
	}
}]);