newspaper.factory('userService', ['$http','$location','$sce','sessionService', function($http, $location, $sce, sessionService) {
	return {
		get: function (linky, scope){
			var packet = {id : sessionService.get('id'), contents: {link : linky}};
			var $promise = $http.post(linkster + 'index.php/user/get',packet);
			$promise.then(function(response) {
				if (response.data) {
					console.log(response.data);
					if (!response.data.success) {
						console.log('server error in get user');
						$location.path('/login');
					} else {
						if(response.data.package.logged == true) {
							scope.main.loggedin = true;
						}
						var userData = response.data.package;
						scope.profileUser.first = userData.first;
						scope.profileUser.last = userData.last;
						scope.profileUser.title = userData.title;
						scope.profileUser.articles = userData.articles;
						scope.main.articleRepo.push(userData.articles);
						scope.main.loaded = true;
					}
				} else {
					console.log('server error');
				}
			});
		},
		set: function (scope){
			var packet = {id:'', contents: scope.newUser};
			var $promise = $http.post(linkster + 'index.php/user/set',packet);
			$promise.then(function(response) {
				console.log(response.data);
				if (response.data) {
					if (!response.data.success) {
						scope.msgtxt = response.data.error;
						console.log(scope.msgtxt);
					} else {
						// create session
						// route to profile
						sessionService.set('id',response.data.package.id);
						$location.path('/profile/' + response.data.package.link);
					}
				} else {
					console.log('no data');
					// set error messages
				}
			});
		},
		login: function (scope) {
			var packet = {id:'', contents: scope.oldUser};
			var $promise = $http.post(linkster + 'index.php/user/login',packet);
			$promise.then(function(response){
				if (response.data) {
					if (!response.data.success) {
						scope.msgtxt=response.data.error;
						$location.path('/login');
					} else {
						// create session
						// route to profile
						sessionService.set('id',response.data.package.id);
						$location.path('/profile/' + response.data.package.link);
					}
				} else {
					console.log('no data');
					// set error messages
				}
			});
		},
		change: function (scope) {
			
		}
	}
}]);