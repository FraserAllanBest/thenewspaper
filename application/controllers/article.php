<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Article extends CI_Controller {

	function __construct() {
		parent::__construct();
		// Load all models and classes on start
		$this->load->model('article_model');
		$this->load->model('article_class');
		$this->load->model('user_model');
		session_start();
    }
    
    // Create New Article
	public function set() {
		
		// Get New Article Information
		$packet = json_decode(file_get_contents('php://input'));
		$packetArticle = $packet->contents;
		$non_post = array(
        	'title' => $packetArticle->title,
        	'description' => $packetArticle->description
		);
		
		// Form Validation of Article Information
	    $_POST = $non_post;
     	$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'required|is_unique[articles.title]');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$response = array(
			'error' => '',
			'success' => FALSE,
			'package' => array()
		);
		
		// Validate creteria, cancel and alert user
		if ($this->form_validation->run() == FALSE) {
			$response['error'] = validation_errors();
			echo json_encode($response);
		} else {
			// Get Title and description
			$title = $this->input->post('title');
			$description = $this->input->post('description');
			
			// Create A New Article
			//// check if login is NULL iff error
			$article = $this->article_model->newArticle($title,$description);
			
			// Respond with login status with link to new article
			if ($this->user_model->loggedIn($packet->id)) {
				$response['success'] = TRUE;
				$response['package'] = array ('logged' => TRUE,'link' => $article->link);
				echo json_encode($response);
			} else {
				$response['success'] = TRUE;
				$response['package'] = array ('logged' => FALSE,'link' => $article->link);
				echo json_encode($response);
			}
		}
	}
	
	public function get() {
		$packet = json_decode(file_get_contents('php://input'));
		$packetLink = $packet->contents;
		$packetId = $packet->id;
		$response = array(
			'error' => '',
			'success' => FALSE,
			'package' => array()
		);
		$gotArticle = $this->article_model->getArticle($packetLink);
		
		if(!is_null($gotArticle)) {
			$response['success'] = TRUE;
			if ($this->user_model->loggedIn($packetId)) {
				$response['package'] = array ('logged' => TRUE,'article' => $gotArticle);
			} else {
				$response['package'] = array ('logged' => FALSE,'article' => $gotArticle);
			}
			echo json_encode($response);
		} else {
			$response['error'] = 'Article not found!';
			echo json_encode($response);
		}
	}
	
	public function save() {
		$packet = json_decode(file_get_contents('php://input'));
		$response = array(
			'error' => '',
			'success' => FALSE,
			'package' => array()
		);
		if (isset($packet->contents) && $this->user_model->loggedIn($packet->id)) {
			$newInfo = array(
				'id' => $packet->contents->id,
				'title' => $packet->contents->title,
				'author' => $packet->contents->author,
				'description' => $packet->contents->description,
				'content' => $packet->contents->content,
				//'nodes' => json_encode($packet->contents->nodes),
				'state' => $packet->contents->state,
				'link' => $packet->contents->link,
				'date' => $packet->contents->date,
				'root' => $packet->contents->root,
				'images' => json_encode($packet->contents->images),
				'featured' => $packet->contents->featured,
				'section' => $packet->contents->section
			);
			$this->article_model->updateArticle($newInfo);
			$response['success'] = TRUE;
			echo json_encode($response);
		} else {
			$response['success'] = False;
			$response['error'] = 'Article not found! or Not logged in';
			echo json_encode($response);
		}
		
	}
	/*
	public function newsave() {
		$packet = json_decode(file_get_contents('php://input'));
		$response = array(
			'error' => '',
			'success' => FALSE,
			'package' => array()
		);
		if (isset($packet->contents->change)) {
			$packetChange = $packet->contents->change;
			$packetLink = $packet->contents->link;
			if ($this->user_model->loggedIn($packet->id)) {
				$gotArticle = $this->article_model->getArticle($packetLink);
				if (isset($gotArticle)){
					$nodes = $gotArticle->nodes;
					foreach ($packetChange as $change) {

						if ($change->idx > (sizeof($nodes)-1)){
							for ($i = 0; $i < $change->idx - sizeof($nodes); $i++){
								array_push($nodes,'NULL');
							}
						}
						$nodes[$change->idx] = $change->elem2;
					}
					
					$this->article_model->updateNodes($nodes,$gotArticle->id);
				} else {
					$response['error'] = 'Article No Longer Exists';
					echo json_encode($response);
				}
				
			} else {
				$response['error'] = 'Not Logged In!';
				echo json_encode($response);
			}
		} else {
		}
		
	}
	*/

}