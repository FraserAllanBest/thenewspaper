<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Image extends CI_Controller {

	function __construct() {
		// Call the Controller constructor
		parent::__construct();
		$this->load->model('article_model');
		$this->load->model('image_model');
		$this->load->model('article_class');
		$this->load->model('user_model');
		session_start();
	}
	
	public function imageUpload() {
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'required');
		
		$response = array(
			'error' => '',
			'success' => FALSE,
			'package' => array()
		);
		if ($this->form_validation->run() == FALSE) {
			$response['error'] = validation_errors();
			echo json_encode($response);
		} else {
			$makeIMG = $this->image_model->set($_FILES['file']);
			if (!is_null($makeIMG)) { 
				$response = array(
					'error' => 'hey',
					'success' => TRUE,
					'package' => array( 'imglink' => $makeIMG)
				);
				echo json_encode($response);
			} else {
				$response = array(
					'error' => 'Internal Error',
					'success' => FALSE,
					'package' => array()
				);
				echo json_encode($response);
			}
			

			
		}
	}
	
}