<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Search extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		
		// Load all models and classes on start
		$this->load->model('article_model');
		$this->load->model('article_class');
		$this->load->model('user_model');
		$this->load->model('search_model');
		session_start();
    }
    
    public function get() {
		$packet = json_decode(file_get_contents('php://input'));
		
		$searchTerms = $packet->contents->terms;
		$searchStageLen = $packet->contents->space;
		$searchIndex = $packet->contents->space * $packet->contents->stages;
		
		// get articles base on terms
		$articles = $this->search_model->get($searchTerms);
		$sorted = $this->search_model->sort($articles);
		// sort articles into content, featured, recent
		// send back to user in sorted array
		$response = array(
			'error' => '',
			'success' => FALSE,
			'package' => array()
		);
		if (isset($sorted)) {
			$response['success'] = TRUE;
			$response['package'] = array('main' => $sorted[0], 'recent' => $sorted[1]);
			echo json_encode($response);
		}

	}
	
	
}