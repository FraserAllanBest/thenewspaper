<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Controller {

    function __construct() {
		// Call the Controller constructor
    	parent::__construct();
    	$this->load->model('User_model');
    	$this->load->model('User_class');
    	$this->load->model('Article_model');
    	session_start();
    	
    }
	
	function login() {
		$packet = json_decode(file_get_contents('php://input'));
		$oldUser = $packet->contents;
		$non_post = array(
        	'email' => $oldUser->email,
        	'password' => $oldUser->password
      	);
		$_POST = $non_post;
		// Form validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
    	$this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[32]|min_length[4]');
    	
    	$response = array(
			'error' => '',
			'success' => FALSE,
			'package' => array()
		);

		if($this->form_validation->run() == FALSE) {
			$response['error'] = validation_errors();
			echo json_encode($response);
		} else {
    		$email = $this->input->post('email');
    		$clearPassword = $this->input->post('password');
    		$member = $this->User_model->getFromEmail($email);
    		if (isset($member) && $member->comparePassword($clearPassword)) {
    			
    			$_SESSION['id'] = $member->id;
    			$response['success'] = TRUE;
				$response['package'] = array (
					'id' => $member->id,
					'link' => $member->link
				);
				echo json_encode($response);
    		} else {
				$response['error'] = 'Invalid Username or Password';
				echo json_encode($response);
    		}
    	}
	}

	function set() {
		$packet = json_decode(file_get_contents('php://input'));
		$newUser = $packet->contents;
		
		$non_post = array(
        	'first' => $newUser->first,
        	'last' => $newUser->last,
        	'email' => $newUser->email,
        	'title' => $newUser->title,
        	'password' => $newUser->password1,
        	'passconf' => $newUser->password2
      	);
		$_POST = $non_post;
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('first', 'First Name', 'trim|required');
		$this->form_validation->set_rules('last', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('title', 'Job Title', 'trim|required');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[passconf]|min_length[4]|max_length[32]');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required');
		
		$response = array(
			'error' => '',
			'success' => FALSE,
			'package' => array()
		);
		if($this->form_validation->run() == FALSE) {
			$response['error'] = validation_errors();
			echo json_encode($response);
		} else {
			$member = new User_class();
			$member->email = $this->input->post('email');
			$member->first = $this->input->post('first');
			$member->last = $this->input->post('last');
			$clearPassword = $this->input->post('password');
			$member->encryptPassword($clearPassword);
			$member->title = $this->input->post('title');
			$member->generateLink();
			
			$insertError = $this->User_model->insert($member);
			$createdMember = $this->User_model->getFromEmail($member->email);

			if ($insertError != 1) {
				$response['error'] = 'server error';
				echo json_encode($response);
			} else {
				$_SESSION['uid'] = $member->id;
				$response['success'] = TRUE;
				$response['package'] = array (
					'id' => $createdMember->id,
					'link' => $createdMember->link
				);
				echo json_encode($response);
				//echo $response['success'];
			}
		}
	}
	
	function get() {
		$packet = json_decode(file_get_contents('php://input'));
		$userLink = $packet->contents->link;
		$userId = $packet->id;
		
		$response = array(
			'error' => '',
			'success' => FALSE,
			'package' => array()
		);
		$getMember = $this->User_model->getFromLink($userLink);
		
		
		if ($getMember){
			
			if ($this->User_model->loggedIn($userId)){
				$articles = $this->Article_model->getArticlesByAuthor($userId);
				$response['success'] = TRUE;
				$response['package'] = array (
					'logged' => TRUE,
					'first' => $getMember->first,
					'last' => $getMember->last,
					'email' => $getMember->email,
					'title' => $getMember->title,
					'articles' => $articles
				);
				echo json_encode($response);
			} else {
				$articles = $this->Article_model->getArticlesByAuthor($getMember->id);
				$response['success'] = TRUE;
				$response['package'] = array (
					'logged' => FALSE,
					'first' => $getMember->first,
					'last' => $getMember->last,
					'email' => $getMember->email,
					'title' => $getMember->title,
					'articles' => $articles
				);
				echo json_encode($response);
			}
			
		} else {
			$response['error'] = 'Cannot find user';
			echo json_encode($response);
		}
		
		
		//echo 'hey';
		
	}

	function updatePasswordForm() {

	}

	function updatePassword() {

	}

	function recoverPassowrdForm() {

	}

	function recoverPassword() {

	}

	function check_session() {

	}

	function logout() {
		/*
		session_id('uid');
		session_destroy();
		session_commit();
		*/
	}
}