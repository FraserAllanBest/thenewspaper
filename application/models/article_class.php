<?php
class Article_class {
	public $id;
	public $title;
	public $author;
	public $content;
	public $nodes;
	public $description;
	public $date;
	public $state;
	public $link;
	public $images;
	public $root;
	public $section;
	public $featured;
	
	public function compile () {
		if (!is_array($this->images)) {
			$images = json_decode($this->images);
		} else {
			$images = $this->images;
		}
		$this->images = $images;
	}
	
	public function decompile(){
		$this->images = json_encode($this->images);
	}
	
	public function returnLink($title) {
		return preg_replace("/[^A-Za-z0-9]/", '_', $title);
	}

	function isJson($string) {
	 	json_decode($string);
	 	return (json_last_error() == JSON_ERROR_NONE);
	}
	
}