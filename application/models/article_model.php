<?php
class Article_model extends CI_Model {
	function newArticle($title,$description) {

		// Load Article class to make new instance of an article
		$this->load->model('article_class');
		// Get a user for the article
		$this->load->model('user_model');
		
		// Create Article Object
		
		$newArticle = new article_class();
		
		// Set Date Of Article Creation
		//// set it to a format that is the same format produces in angular
		$newArticle->date = date("Y/m/d");
		// Get author from session and return full name
		
		$newArticle->author = $this->user_model->getFromId($_SESSION['id'])->fullName();
		// Create an array of nodes and images
		//$newArticle->nodes = array("<p> New Article ! </p>");
		$newArticle->content = "<p> New Article ! </p>";
		$newArticle->nodes = '';
		$newArticle->section = 'main';
		$newArticle->featured = false;
		
		$newArticle->images = array();
		// Compile nodes to content
		$newArticle->compile();
		$newArticle->description = $description;
		$newArticle->title = $title;
		// Make a link out of title
		
		$newArticle->link = $newArticle->returnLink($newArticle->title);
		$newArticle->state = "draft";
		
		// Make directory for file 
		$newArticle->root = 'uploads/' . $newArticle->link;
		mkdir($newArticle->root);
		
		// Decompile by turning Images and Nodes into JSON
		$newArticle->decompile();

		$this->db->insert('articles',$newArticle);

		// Get id from inserted article 
		$newArticle->id = $this->getArticle($newArticle->link)->id;
		
		// Recompile to export to front end
		$newArticle->compile(); 
		
		
		return $newArticle;
		
		
	}
	
	function getArticlesByAuthor($id) {
		
		$this->load->model('article_class');
		$this->load->model('user_model');
		
		$this->db->where('author', $this->user_model->getFromId($id)->fullName());
		$query = $this->db->get('articles');
		$articles = array();
		foreach ($query->result() as $row) {
	   		$article = new article_class();
	    	$article->title = $row->title;
	    	$article->author = $row->author;
	    	$article->date = $row->date;
	    	$article->content = $row->content;
	    	$article->images = $row->images;
	    	$article->link = $row->link;
	    	$article->state = $row->state;
	    	$article->description = $row->description;
	    	$article->root = $row->root;
	    	$article->id = $row->id;
	    	$article->section = $row->section;
	    	$article->featured = $row->featured;
	    	// compiles images To Array
	    	$article->compile();
	    	array_push($articles,$article);
	   }
	   // Return articles by that author
	   return $articles;
	}
	
	function getArticle($link) {
		$this->load->model('article_class');
		$this->db->where('link', $link);
		$query = $this->db->get('articles');
		if($query && ($query->num_rows() == 1)) {
			$gotarticle = $query->row(0,'article_class');
			$gotarticle->compile();
			return $gotarticle;
		} else {
			return null;
		}
	}
	function getAuthorByArticleID($id) {
		$this->load->model('article_class');
		$this->db->where('id', $id);
		$query = $this->db->get('articles');
		if($query->num_rows == 1) {
			return $query->row(0,'article_class')->author;
		} else {
			return null;
		}
	}
	function updateArticle($data){
		$this->db->where('id', $data['id']);
		$this->db->update('articles', $data);
	}
	/*
	function updateNodes($nodes,$id) {
		$data = array( 'nodes' => json_encode($nodes));
        $this->db->where('id', $id);
		$this->db->update('articles', $data);
	}
	*/
}
