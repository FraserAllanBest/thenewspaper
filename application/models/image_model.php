<?php
class Image_model extends CI_Model {
	
	public function set($image) {
		
		// Create Article Title
		$articleFileName = $_POST['link'];
		$imageTitle = preg_replace("/[^A-Za-z0-9]/", '_', $_POST['title']) . '_' . rand(pow(10, 3-1), pow(10, 3)-1);
		
		// Check If Accepted Format
		$acceptedFormats = array('jpg','jpeg','png','bmp');
		$imageFormat = '' ;
		
		foreach ($acceptedFormats as $format){
			$imageFormat = $format;
			break;
		}
		//print_r($image);
		if ($imageFormat) {
			// Rename Image
			$image['name'] = $imageTitle . '.' . $imageFormat ;
			
			// Make Directory For Image
			$imageFilePath = 'uploads/'. $articleFileName .'/'. $imageTitle;
			mkdir($imageFilePath);
			
			// Move Image to directory
			move_uploaded_file( $image['tmp_name'] , $imageFilePath . '/' . $image['name'] );
			
			// Create Featured picture
			$config['image_library'] = 'GD2';
			$config['source_image']	= $imageFilePath . '/' . $image['name'];
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = TRUE;
			$config['new_image'] = $imageFilePath . '/' . $imageTitle . '.' . $imageFormat;
			$config['height'] = 1000;
			$config['width'] = 750;

			$this->load->library('image_lib', $config);
			$this->image_lib->resize();

			$this->image_lib->clear();

			$config2['image_library'] = 'GD2';
			$config2['source_image']	= $imageFilePath . '/' . $image['name'];
			$config2['create_thumb'] = FALSE;
			$config2['maintain_ratio'] = TRUE;
			$config2['new_image'] = $imageFilePath . '/' . $imageTitle . '_featured.' . $imageFormat;
			$config2['height'] = 500;
			$config2['width'] = 350;

			$this->load->library('image_lib', $config2);
			$this->image_lib->resize();
			$this->image_lib->clear();

			$config3['image_library'] = 'GD2';
			$config3['source_image']	= $imageFilePath . '/' . $image['name'];
			$config3['create_thumb'] = FALSE;
			$config3['maintain_ratio'] = TRUE;
			$config3['new_image'] = $imageFilePath . '/' . $imageTitle . '_thumb.' . $imageFormat;
			$config3['height'] = 100;
			$config3['width'] = 100;

			$this->load->library('image_lib', $config3);
			$this->image_lib->resize();
			$this->image_lib->clear();

		
			return 'http://localhost:8888/newspaper/' . $imageFilePath . '/' . $imageTitle .  '.' . $imageFormat;
			
		} 
		else {
			return NULL;
		}
		
		// thumbnail 
		// crop 
		// resize
		
		// featured picture

	}
	
	public function updateArticleImg($artlink,$imglink) {
		
		$this->load->model('article_model');
		$article = $this->article_model->getArticle($artlink);
		
		$article->compile();
		array_push($article->images,$imglink);
		$article->decompile();
		$data = array(
			'images' => $article->images
		);
		
		$this->db->where('id', $article->id);
		$this->db->update('articles', $data);
	}
}