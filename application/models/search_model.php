<?php
class Search_model extends CI_Model {
	
	function get($terms) {
		$this->load->model('article_class');
		// Main gets all the articles
		if(isset($terms[0]) && $terms[0]!='main'){
			$this->db->where('section',$terms[0]);
		}
		$query = $this->db->get('articles');
		$articles = array();
		foreach ($query->result() as $row) {
	   		$article = new article_class();
	    	$article->title = $row->title;
	    	$article->author = $row->author;
	    	$article->date = $row->date;
	    	$article->content = $row->content;
	    	$article->images = $row->images;
	    	$article->link = $row->link;
	    	$article->state = $row->state;
	    	$article->description = $row->description;
	    	$article->root = $row->root;
	    	$article->id = $row->id;
	    	$article->section = $row->section;
	    	$article->featured = $row->featured;
	    	// compiles images To Array
	    	$article->compile();
	    	array_push($articles,$article);
	   }
	   //print_r($articles);
	   return $articles;
	}

	function sort($articles) {
		
		$mainSort = [];
		$mainTemp = [];
		$main = [];
		$recentSort = 0;
		$recent = [];
		function dateSort($a,$b) {
		 	if (date($a->date) == date($b->date)){
		 		return 0;
		 	}
		 	return (date($a->date) < date($b->date)) ? -1 : 1;
		}
		usort($articles,"dateSort");
		$total = count($articles);
		$choose = rand(2,4);
		if ($total < 3) {
			$main = $articles;
		} else {
			for ($i = $total; $i > 0; $i -= $choose+1){
				if ($i == $total) {
					$choose = 3;
					array_push($mainSort,$choose);
				} else if ($i < 5) {
					$choose = $i;
					array_push($mainSort,$choose);
				} else {
					$choose = rand(2,4);
					array_push($mainSort,$choose);
				}
			}
			
			for($i = 0; $i < count($articles); $i++){
				array_push($mainTemp,$articles[$i]);
			}
			for($i = 0; $i < count($mainSort); $i++) {
				$array = array();
				for ($j = 0; $j < $mainSort[$i]; $j++) {
					array_push($array,array_pop($mainTemp));
				}
				array_push($main,$array);
			}
			array_push($main,$mainTemp);
		}
		return [$main,$recent];
	}
}