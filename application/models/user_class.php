<?php 
class User_class {

	// Variables
	
	public $id;
	public $first;
	public $last;
	public $email;
	public $link;
	public $password; // hashed version
	public $salt;
	public $title;

	// Functions

	public function encryptPassword($clearPassword) {
		$this->salt = mt_rand();
		$this->password = sha1($this->salt . $clearPassword);
	}

	// Initialize the password to a random value
	public function initPassword() {
		$this->salt = mt_rand();
		$clearPassword = mt_rand();
		$this->password = sha1($this->salt . $clearPassword);
		return $clearPassword;
	}

	public function comparePassword($clearPassword) {
		if ($this->password == sha1($this->salt . $clearPassword)) {
			return true;
		}
		return false;
	}
	
	public function generateLink() {
		return $this->link = $this->first . '_' . $this->last . '_' . strval(rand ( 0 , 10));
	}

	public function fullName() {
		return $this->first . ' ' . $this->last;
	}
}