<?php
	class User_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }
	function getFromEmail($email) {
		$this->load->model('User_Class');
		$this->db->where('email', $email);
		$query = $this->db->get('membership');

		if ($query && $query->num_rows() > 0) {
			return $query->row(0,'User_Class');
		} else {
			return null;
		}
	}
	function getFromId($id) {
		$this->load->model('User_Class');
		$this->db->where('id', $id);
		$query = $this->db->get('membership');
		if( $query && ($query->num_rows() == 1)) {
			return $query->row(0,'User_Class');
		} else {
			return null;
		}
	}
	function getFromLink($link) {
		$this->load->model('User_Class');
		$this->db->where('link', $link);
		$query = $this->db->get('membership');
		if( $query && ($query->num_rows() == 1)) {
			return $query->row(0,'User_Class');
		} else {
			return null;
		}
	}
	function insert($member) {
		return $this->db->insert('membership', $member);
	}
	function loggedIn($id) {
		if (isset($_SESSION['id']) && ($_SESSION['id'] == $id)){
			return true;
		} else {
			return false;
		}
	}
		
}