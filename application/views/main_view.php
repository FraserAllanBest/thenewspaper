<!DOCTYPE html>
<html lang="en" ng-app="newspaper">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular.min.js"></script>
	<title ng-bind="main.title">The Newspaper</title>
    <!-- Bootstrap -->
	<link rel="stylesheet" href="<?php echo base_url() ?>bootstrap/css/newspaperOld.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>bootstrap/css/newspaper.css">
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,400italic,500,700,300italic,500italic,700italic' rel='stylesheet' type='text/css'>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body  ng-cloak>
	<base href="/">
	<div ng-controller="MainController" class="np-container" ng-show="main.loaded">
		<!-- Header -->
		<np-header collection="page.sections" theme="theme"></np-header>
		<np-search ng-model="searchy.value"></np-search>
	    <div ng-view></div>
	    
	</div>
	<script type="text/javascript" src="<?php echo base_url()?>angular/app.min.js"></script>
  </body>
</html>